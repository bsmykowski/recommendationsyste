package recommenderSystem.users;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsersCollection {

    private Map<Integer, User> users = new HashMap<>();

    public UsersCollection(List<String> ranks){
        for(String line: ranks){
            String[] tokens = line.split("\t");
            int userId = Integer.parseInt(tokens[0]);
            int itemId = Integer.parseInt(tokens[1]);
            int rank = Integer.parseInt(tokens[2]);
            addRank(userId, itemId, rank);
        }

        for(User user: users.values()){
            user.normalizeRanks();
        }
    }

    private void addRank(int userId, int itemId, int rank){
        if(users.containsKey(userId)){
            users.get(userId).addRank(itemId, rank);
        } else{
            User newUser = new User(userId);
            newUser.addRank(itemId, rank);
            users.put(userId, newUser);
        }
    }

    public User getUser(int userId){
        return users.get(userId);
    }

    public Collection<User> getUsers(){
        return users.values();
    }

}
