package recommenderSystem.users;

import lombok.Getter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.DoubleStream;

public class User {

    @Getter
    private int id;
    private Map<Integer, Integer> ranks = new HashMap<>();
    private Map<Integer, Double> normalizedRanks = new HashMap<>();
    private double avg = Double.NaN;
    private double sumOfNormalizedRanks = Double.NaN;

    public User(int id){
        this.id = id;
    }

    public void addRank(int itemId, int rank){
        ranks.put(itemId, rank);
    }

    public double getRanksAvg(){
        if(Double.isNaN(avg)){
            avg = countAvg();
        }
        return avg;
    }

    public double getSumOfNormalizedRanks(){
        if (Double.isNaN(sumOfNormalizedRanks)){
            sumOfNormalizedRanks = normalizedRanks
                    .values()
                    .stream()
                    .flatMapToDouble(r -> DoubleStream.of(Math.pow(r, 2)))
                    .sum();
        }
        return sumOfNormalizedRanks;
    }

    private double countAvg() {
        double sum = 0;
        for(Integer rank : ranks.values()){
            sum += rank;
        }
        return sum/ranks.size();
    }

    public void normalizeRanks(){
        double avg = getRanksAvg();
        for(Integer itemId : ranks.keySet()){
            normalizedRanks.put(itemId, ranks.get(itemId) - avg);
        }
    }

    public double getNormalizedRank(int itemId){
        return normalizedRanks.get(itemId);
    }

    public Collection<Integer> getItems(){
        return ranks.keySet();
    }

    public int getRank(int itemId){
        return ranks.get(itemId);
    }

    public boolean hasRank(int itemId){
        return ranks.containsKey(itemId);
    }

}
