package recommenderSystem;

import lombok.Getter;
import recommenderSystem.users.User;
import recommenderSystem.users.UsersCollection;

import java.util.List;

public class RecommendationTester {

    private UsersCollection testUsers;
    private Recommender recommender;
    @Getter
    private double MAE = 0.0;
    @Getter
    private double RMSE = 0.0;

    public RecommendationTester(List<String> baseData, List<String> testData){
        UsersCollection baseUsersCollection = new UsersCollection(baseData);
        this.testUsers = new UsersCollection(testData);
        this.recommender = new Recommender(baseUsersCollection);
    }

    public void performTest(int maxNumberOfSimilarUsers){
        double errorsSum = 0.0;
        double errorsPowerSum = 0.0;
        int numberOfTests = 0;
        for(User testUser: testUsers.getUsers()){
            for(Integer testItemId: testUser.getItems()){
                numberOfTests++;
                double prediction = recommender.getPrediction(testUser.getId(), testItemId, maxNumberOfSimilarUsers);
                if(!Double.isNaN(prediction)){
                    errorsSum += Math.abs(testUser.getRank(testItemId) - prediction);
                    errorsPowerSum += Math.pow(testUser.getRank(testItemId) - prediction, 2);
                }
//                if(numberOfTests%10000 == 0){
//                    System.out.println(numberOfTests);
//                }
            }
        }
        MAE = errorsSum/numberOfTests;
        RMSE = Math.sqrt(errorsPowerSum/numberOfTests);
    }

}
