package recommenderSystem;

import recommenderSystem.users.User;
import recommenderSystem.users.UsersCollection;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Recommender {

    private UsersCollection usersCollection;
    private Map<Integer, Map<Integer, Double>> similarities = new HashMap<>();

    public Recommender(UsersCollection usersCollection){
        this.usersCollection = usersCollection;
    }

    public double getPrediction(int userId, int itemId, int maxNumberOfSimilarUsers){
        User userToPredict = usersCollection.getUser(userId);
        double userRanksAvg = userToPredict.getRanksAvg();

        if(userToPredict.hasRank(itemId)){
            return userToPredict.getRank(itemId);
        }

        List<User> similarUsers = usersCollection
                .getUsers()
                .stream()
                .filter(u -> u.hasRank(itemId))
                .sorted(Comparator.comparingDouble(o -> -getPearsonSimilarity(o, userToPredict)))
                .limit(maxNumberOfSimilarUsers)
                .collect(Collectors.toList());

        double overLine = 0;
        double sum = 0;
        for(User similarUser: similarUsers){
            double pearsonSimilarity = getPearsonSimilarity(userToPredict, similarUser);
            overLine += pearsonSimilarity *(similarUser.getNormalizedRank(itemId));
            sum += pearsonSimilarity;
        }

        return userRanksAvg + overLine/sum;
    }

    private double getPearsonSimilarity(User user1, User user2) {
        int smallerId = Math.min(user1.getId(), user2.getId());
        int biggerId = Math.max(user1.getId(), user2.getId());
        if(similarities.containsKey(smallerId)){
            if(similarities.get(smallerId).containsKey(biggerId)){
                return similarities.get(smallerId).get(biggerId);
            }
        }

        double overLine = user1
                .getItems()
                .stream()
                .filter(user2::hasRank)
                .flatMapToDouble(itemId -> DoubleStream.of(user1.getNormalizedRank(itemId) * user2.getNormalizedRank(itemId)))
                .sum();
        double sum1 = user1.getSumOfNormalizedRanks();
        double sum2 = user2.getSumOfNormalizedRanks();

        double result = overLine/(Math.sqrt(sum1)*Math.sqrt(sum2));

        smallerId = Math.min(user1.getId(), user2.getId());
        biggerId = Math.max(user1.getId(), user2.getId());
        similarities.putIfAbsent(smallerId, new HashMap<>());
        similarities.get(smallerId).putIfAbsent(biggerId, result);

        return result;
    }

}
