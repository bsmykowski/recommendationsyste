package recommenderSystem;

import java.util.List;

public class App {

    public static void main(String[] args) {
        List<String> lines = FilesReader.getLines("src/main/resources/u1.base");
        List<String> testLines = FilesReader.getLines("src/main/resources/u1.test");

        RecommendationTester tester = new RecommendationTester(lines, testLines);

        //11
        for(int i = 1; i <= 20; i++) {
            System.out.println("-----------" + i + "------------");
            long start = System.nanoTime();
            tester.performTest(i);
            System.out.println("MAE " + tester.getMAE());
            System.out.println("RMSE " + tester.getRMSE());
            System.out.println("time: " + (System.nanoTime() - start) / 1000000000.0);
        }
    }

}
